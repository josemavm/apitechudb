package com.techu.apitechudb.services;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.repositories.UserRepository;
import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    public List<UserModel> getUsers(String orderBy){
        System.out.println("fidAll Userservice");

        List<UserModel> result;
        if (orderBy != null){
            System.out.println("Se ordenará");
            result= this.userRepository.findAll(Sort.by("age"));
        }else{
            result= this.userRepository.findAll();
        }

        return  result;
    }

    public Optional<UserModel> findById(String id){
        System.out.println("findById");
        return this.userRepository.findById(id);

    }

    public UserModel add(UserModel user){
        System.out.println("add UserService");
        return this.userRepository.save(user);
    }

    public UserModel updateUser(UserModel user){
        System.out.println("update");
        return this.userRepository.save(user);
    }

    public boolean delete(String id){
        boolean result= false;
        System.out.println("delete en userService");
        if(this.findById(id).isPresent() == true) {
            System.out.println("User a eliminar encontrado");
            this.userRepository.deleteById(id);
            result=true;
        }
        return result;

    }

}
