package com.techu.apitechudb.services;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;
import java.util.Optional;

@Service
public class ProductService {
    @Autowired
    ProductRepository productRepository;

    public List<ProductModel> findAll(){
        System.out.println("fidAll Producservice");

        return  this.productRepository.findAll();
    }

    public ProductModel add(ProductModel product){
        System.out.println("add product service");
        return this.productRepository.save(product);
    }

    public Optional<ProductModel>  findById(String id){
        System.out.println("findByID");
        return this.productRepository.findById(id);

    }

    public ProductModel update(ProductModel product){
        System.out.println("update");
        return this.productRepository.save(product);
    }

    public boolean delete(String id){

        boolean result= false;

        System.out.println("delete en productService");

        if(this.findById(id).isPresent() == true) {
            System.out.println("Producto a eliminar encontrado");
            this.productRepository.deleteById(id);
            result=true;

        }

        return result;

    }



}
