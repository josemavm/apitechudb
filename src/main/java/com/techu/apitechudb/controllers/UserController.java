package com.techu.apitechudb.controllers;


import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping("/users")
    public ResponseEntity<List<UserModel>> getUser(
            @RequestParam(name="$orderby", required = false) String orderBy
    ){
        System.out.println("getUser");
        System.out.println("El valor del orderBy es " + orderBy);

        return new ResponseEntity<>(
                this.userService.getUsers(orderBy), HttpStatus.OK
        );
    }

    @GetMapping("/users/{id}")
    public ResponseEntity<Object> getUserById(@PathVariable String id){
        System.out.println("getUserById");
        System.out.println("La id del usuario a buscar es " + id);

        Optional<UserModel> result = this.userService.findById(id);

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "Usuario no encontrado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );

    }


    @PostMapping("/users")
    public ResponseEntity<UserModel> addUser(@RequestBody UserModel user){
        System.out.println("addUser");

        System.out.println("Id del usuario a crear" + user.getId());
        System.out.println("Nombre del usuario a crear" + user.getName());
        System.out.println("Edad del usuario a crear" + user.getAge());

        return  new ResponseEntity<>(
                this.userService.add(user),HttpStatus.CREATED
        );
    }

    @PutMapping("/users/{id}")
    public ResponseEntity<UserModel> updateUser(@PathVariable String id, @RequestBody UserModel user){
        System.out.println("updateUser");
        System.out.println("La id del usuario que se va actualizar en el parametro URL es " + id);
        System.out.println("La id del usuario que se va actualizar es " + user.getId());
        System.out.println("El nombre del usuario que se va a modificar es " + user.getName());
        System.out.println("La edad del usurio que se va a modificar es " + user.getAge());

        Optional<UserModel> userToUpdate= this.userService.findById(id);

        if(userToUpdate.isPresent()){
            System.out.println("Usurio encontrado, actulizado");

            this.userService.updateUser(user);
        }

        return new ResponseEntity<>(
               user, userToUpdate.isPresent() ? HttpStatus.OK:HttpStatus.NOT_FOUND
        );

    }




    @DeleteMapping("/users/{id}")
    public ResponseEntity<String> deleteUser(@PathVariable String id){

        System.out.println("deleteUser");

        boolean deleteUser = this.userService.delete(id);

        return new ResponseEntity<>(
                deleteUser ? "Usuario borrado" :"Usuario no borrado",
                deleteUser ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

}
